import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Post } from '../common/interfaces/post.interface';
import { User } from '../common/interfaces/user.interface';
import { ToolbarService } from '../common/services/toolbar.service';

@Component({
  selector: 'post-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.css']
})
export class PostDialogComponent {

  post!:Post;
  user!:User;
  theme$ = 'default';

  constructor(@Inject(MAT_DIALOG_DATA) data: any, private toolbarService: ToolbarService){
    this.post = data.post;
    this.user = data.user;
  }

  ngOnInit(){
    this.toolbarService.getTheme().subscribe(t => this.theme$ = t);
  }

}
