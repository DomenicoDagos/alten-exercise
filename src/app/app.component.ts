import { Component } from '@angular/core';
import { ToolbarService } from './common/services/toolbar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'alten-exercise';
  theme$ = 'default';

  constructor(private service: ToolbarService){}

  ngOnInit(){
    this.service.getTheme().subscribe(t => this.theme$ = t);
  }
}
