import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../common/services/user.service';
import { User } from '../common/interfaces/user.interface';
import { switchMap } from 'rxjs';

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent {

  user:any;
  loading:boolean = true; 

  constructor(private route: ActivatedRoute, private service: UserService){}

  ngOnInit(){
    this.route.paramMap
    .pipe(
      switchMap(params => {
        let userId = Number(params.get('user-id'));
        
        return this.service.get(userId); 
      })
    ).subscribe({
      next: user => { 
        this.user = user;
        this.loading = false; 
      },
      error: () => {
        this.loading = false;
      }
    });
  }
}
