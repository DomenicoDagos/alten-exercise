import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToolbarService {

  filter = new BehaviorSubject('');
  theme = new BehaviorSubject('default');

  constructor() {}

  updateFilter(input:string){
    this.filter.next(input);
  }
  getFilter(){
    return this.filter.asObservable();
  }

  updateTheme(color:string){
    this.theme.next(color);
  }
  getTheme(){
    return this.theme.asObservable();
  }
  
}
