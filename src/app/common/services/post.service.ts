import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { combineLatest, filter, map } from 'rxjs';
import { ToolbarService } from './toolbar.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  url = 'https://jsonplaceholder.typicode.com/posts';
  filter = '';
  
  constructor(private http: HttpClient, private toolbarService: ToolbarService) {
    this.toolbarService.getFilter().subscribe(f => this.filter = f);
  }

  getAll(){
    return this.http.get(this.url);
  }

  get(postId:number){
    return this.http.get(this.url + '/' + postId);
  }

}
