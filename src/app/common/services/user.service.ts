import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(this.url);
  }

  get(userId:number){
    return this.http.get(this.url + '/' + userId);
  }
}
