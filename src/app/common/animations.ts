import { animate, animateChild, query, stagger, style, transition, trigger } from '@angular/animations';

export let fadeList = [
    trigger('fadeList', [
        transition('* => *', [
        query('.post', [
            stagger(150, animateChild())
        ], { optional: true })
        ])
    ]),
        trigger('fade', [
        transition(':enter',[
        style({ opacity: 0 }),
        animate(500)
        ])
    ])
]