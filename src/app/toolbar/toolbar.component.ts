import { Component } from '@angular/core';
import { ToolbarService } from '../common/services/toolbar.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  filter$ = '';
  theme$ = 'default';

  constructor(private service: ToolbarService){}

  ngOnInit(){
    combineLatest([
      this.service.getFilter(),
      this.service.getTheme()
    ])
    .subscribe(([filter, theme]) => {
      this.filter$ = filter;
      this.theme$ = theme;
    });
  }
  
  updateFilter(word:string){
    this.service.updateFilter(word);
  }

  updateTheme(color:string){
    this.service.updateTheme(color);
  }
}
