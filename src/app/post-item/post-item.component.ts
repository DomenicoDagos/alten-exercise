import { Component, Input } from '@angular/core';
import { Post } from '../common/interfaces/post.interface';
import { User } from '../common/interfaces/user.interface';
import { MatDialog } from '@angular/material/dialog';
import { PostDialogComponent } from '../post-dialog/post-dialog.component';

@Component({
  selector: 'post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})
export class PostItemComponent {
  @Input('post') post?:Post;
  @Input('user') user?:User;
  @Input('isLast') isLast:boolean = false;
  @Input('theme') theme:string = 'default';

  constructor(public dialog: MatDialog){}

  getInitials(){
    let fullName = this.user ? this.user.name.split(' ') : [];
    if (fullName.length > 1) {
      let nameInitizial = fullName[0].length > 0 ? fullName[0].substring(0,1) : 'A';
      let surnameInitial = fullName[1].length > 0 ? fullName[1].substring(0,1) : '';
      return (nameInitizial + surnameInitial).toUpperCase();
    }
    return 'AA'; //If the name does not contain a surname it returnd a default string
  }

  operPostPreview(){
    this.dialog.open(PostDialogComponent, { 
      data: {
        post: this.post,
        user: this.user,
      }
    });
  }
}
