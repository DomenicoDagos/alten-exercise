import { Component } from '@angular/core';
import { PostService } from '../common/services/post.service';
import { Post } from '../common/interfaces/post.interface';
import { UserService } from '../common/services/user.service';
import { User } from '../common/interfaces/user.interface';
import { combineLatest } from 'rxjs';
import { ToolbarService } from '../common/services/toolbar.service';
import { fadeList } from '../common/animations';

@Component({
  selector: 'post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  animations: [
    fadeList
  ]
})
export class PostListComponent {
  posts:any = [];
  users:any = [];
  filter$ = '';
  theme$ = 'default';

  constructor(
    private postService: PostService, 
    private userService: UserService,
    private toolbarService: ToolbarService
  ){}

  ngOnInit(){
    combineLatest([
      this.postService.getAll(),
      this.userService.getAll(),
      this.toolbarService.getFilter(),
      this.toolbarService.getTheme(),
    ])
    .subscribe(([posts, users, filter, theme]) => {
      this.posts = posts
      this.users = users;
      this.filter$ = filter;
      this.theme$ = theme;
    });
  }

  getPosts(){
    return this.posts.filter(( post:Post ) => post.title.includes( this.filter$ ));
  }

  getUser(userId:number){
    return this.users?.find(( user:User ) => user.id === userId);
  }

}
