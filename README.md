# AltenExercise

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.4.

This little "awesome" application was developed for giving a sample of my Angular knowledge. Using the app you can browse through some posts and take a look to the author profile. The app also features a filter, some animations written throw Angular Animation, and allows you to change the color theme.

It is based on an old exercise given to me by a company for testing my skills. Hence, it makes use of services, interfaces, subjects and other features provided by Angular and RxJS.

## Installing and running

Please run `npm install` for installing all the dependencies. Then, run `node node_modules/@angular/cli/bin/ng s` to start the app and navigate to `http://localhost:4200/` for playing with it.